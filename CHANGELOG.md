Changelog
=========

What has been done since last release.

membernator 1.1.0 (20191019)
----------

* fix dependency import issue
* add support for localisation
* show how many valid cards have been scanned


membernator 1.0.1 (20190619)
----------------------------

* add a default for the logfile


membernator 1.0.0 (20190619)
----------------------------

* Initial release
